// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "VRPROJECTGameMode.h"
#include "VRPROJECTHUD.h"
#include "VRPROJECTCharacter.h"
#include "UObject/ConstructorHelpers.h"

AVRPROJECTGameMode::AVRPROJECTGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AVRPROJECTHUD::StaticClass();
}
